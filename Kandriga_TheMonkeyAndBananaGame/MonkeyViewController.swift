//
//  MonkeyViewController.swift
//  Kandriga_TheMonkeyAndBananaGame
//
//  Created by Kandriga,Naga Krishna Lalith on 11/16/16.
//  Copyright © 2016 Kandriga, Lalith. All rights reserved.
//

import UIKit

class MonkeyViewController: UIViewController {

    var origin:CGPoint!
    
    var bananasNumbers:UInt32!
    
    var newCoordsArray:[CGFloat] = [0,0,0,0]
    
    @IBOutlet var bananaUpperLeftIV: UIImageView!
    
    @IBOutlet var bananaUpperRightIV: UIImageView!
    
    @IBOutlet var monkeyCenterIV: UIImageView!
    
    @IBOutlet var bananaLowerLeftIV: UIImageView!
    
    @IBOutlet var bananaLowerRightIV: UIImageView!
    
    @IBOutlet var questionLBL: UILabel!
    
    //function checks the distance of each banana with respect to the center image of the monkey
    @IBAction func resultCheckingBTN(sender: AnyObject) {
        var bananasCount = 0
        
        //formula for finding distance = sqrt((a.x-b.x)^2 + (a.y-b.y)^2)
        
        let newUlBananaCoordsX = (monkeyCenterIV.center.x - bananaUpperLeftIV.center.x) * (monkeyCenterIV.center.x - bananaUpperLeftIV.center.x)
        let newUlBananaCoordsy = (monkeyCenterIV.center.y - bananaUpperLeftIV.center.y) * (monkeyCenterIV.center.y - bananaUpperLeftIV.center.y)
        let ulCoordinates = sqrt(newUlBananaCoordsX + newUlBananaCoordsy)
        newCoordsArray[0] = ulCoordinates
        
        let newUrBananaCoordsX = (monkeyCenterIV.center.x - bananaUpperRightIV.center.x) * (monkeyCenterIV.center.x - bananaUpperRightIV.center.x)
        let newUrBananaCoordsY = (monkeyCenterIV.center.y - bananaUpperRightIV.center.y) * (monkeyCenterIV.center.y - bananaUpperRightIV.center.y)
        let urCoordinates = sqrt(newUrBananaCoordsX + newUrBananaCoordsY)
        newCoordsArray[1] = urCoordinates
        
        let newLlBananaCoordsX = (monkeyCenterIV.center.x - bananaLowerLeftIV.center.x) * (monkeyCenterIV.center.x - bananaLowerLeftIV.center.x)
        let newLlBananaCoordsY = (monkeyCenterIV.center.y - bananaLowerLeftIV.center.y) * (monkeyCenterIV.center.y - bananaLowerLeftIV.center.y)
        let llCoordinates = sqrt(newLlBananaCoordsX + newLlBananaCoordsY)
        newCoordsArray[2] = llCoordinates
        
        let newLrBananaCoordsX = (monkeyCenterIV.center.x - bananaLowerRightIV.center.x) * (monkeyCenterIV.center.x - bananaLowerRightIV.center.x)
        let newLrBananaCoordsL = (monkeyCenterIV.center.y - bananaLowerRightIV.center.y) * (monkeyCenterIV.center.y - bananaLowerRightIV.center.y)
        let lrCoordinates = sqrt(newLrBananaCoordsX + newLrBananaCoordsL)
        newCoordsArray[3] = lrCoordinates
        
        
        for midPt in newCoordsArray {
            if midPt <=  80 {
                bananasCount += 1
            }
        }
        
//        if bananasNumbers == nil {
//            questionLBL.text! = " I haven't asked for bananas yet !!!! "
//        }
        
        if bananasCount == Int(bananasNumbers) {
            
            questionLBL.text! = " Thank you !!!! "
        } else {
            questionLBL.text! = "Lets try again. I asked for \(bananasNumbers) banana(s)"
        }
    }
    
    // function generates a new question
    @IBAction func newProblemBTN(sender: AnyObject) {
        bananasNumbers = arc4random_uniform(4) + 1
        questionLBL.text! = "I want \(bananasNumbers) banana(s)!"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /* Adding these over here so that if user 
         tries to move befor genrating a question, error isnt thrown as
          question gets generated as the view loads
         */
        bananasNumbers = arc4random_uniform(4) + 1
        questionLBL.text! = "I want \(bananasNumbers) banana(s)!"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // function moves the banana image in the upper left corner
    @IBAction func moveMeUL(panGR:UIPanGestureRecognizer)->Void {
        
        
        if panGR.state == UIGestureRecognizerState.Began {
            origin = bananaUpperLeftIV.frame.origin
        }
        print(panGR.translationInView(bananaUpperLeftIV))
        let movedPt:CGPoint = panGR.translationInView(bananaUpperLeftIV)
        
        if origin.y + movedPt.y > 0 {
            bananaUpperLeftIV.frame.origin.x = origin.x + movedPt.x
            bananaUpperLeftIV.frame.origin.y = origin.y + movedPt.y
        }
        
    }
    
    
    // function moves the banana image in the upper right corner
    @IBAction func moveMeUR(panGR:UIPanGestureRecognizer)->Void {
        
        
        if panGR.state == UIGestureRecognizerState.Began {
            origin = bananaUpperRightIV.frame.origin
        }
        print(panGR.translationInView(bananaUpperRightIV))
        let movedPt:CGPoint = panGR.translationInView(bananaUpperRightIV)
        
        if origin.y + movedPt.y > 0 {
            bananaUpperRightIV.frame.origin.x = origin.x + movedPt.x
            bananaUpperRightIV.frame.origin.y = origin.y + movedPt.y
        }
        
    }
    
    // function moves the banana image in the lower left corner
    @IBAction func moveMeLL(panGR:UIPanGestureRecognizer)->Void {
        
        
        if panGR.state == UIGestureRecognizerState.Began {
            origin = bananaLowerLeftIV.frame.origin
        }
        print(panGR.translationInView(bananaLowerLeftIV))
        let movedPt:CGPoint = panGR.translationInView(bananaLowerLeftIV)
        
        if origin.y + movedPt.y > 0 {
            bananaLowerLeftIV.frame.origin.x = origin.x + movedPt.x
            bananaLowerLeftIV.frame.origin.y = origin.y + movedPt.y
        }
        
    }
    
    // function moves the banana image in the lower right corner
    @IBAction func moveMeLR(panGR:UIPanGestureRecognizer)->Void {
        
        
        if panGR.state == UIGestureRecognizerState.Began {
            origin = bananaLowerRightIV.frame.origin
        }
        print(panGR.translationInView(bananaLowerRightIV))
        let movedPt:CGPoint = panGR.translationInView(bananaLowerRightIV)
        
        if origin.y + movedPt.y > 0 {
            bananaLowerRightIV.frame.origin.x = origin.x + movedPt.x
            bananaLowerRightIV.frame.origin.y = origin.y + movedPt.y
        }
        
    }
    
}
